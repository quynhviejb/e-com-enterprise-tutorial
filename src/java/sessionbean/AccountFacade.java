/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entitybean.Account;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author viquy
 */
@Stateless
public class AccountFacade extends AbstractFacade<Account> {

    @PersistenceContext(unitName = "ejBank-beanPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AccountFacade() {
        super(Account.class);
    }
    
    public Account checkAccount(String id) {
        Query query = em.createNamedQuery("Account.findByAccountID");
        query.setParameter("accountID", id);
        try {
            return (Account) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public String payment(String id,double total){
        if(checkAccount(id)!=null){
            Account acc = checkAccount(id);
            double pay = acc.getBalance().doubleValue() - total;
            
            acc.setBalance(Double.valueOf(pay));
            return "Customer "+acc.getAccName()+" pay successful: Your balance now is "+acc.getBalance().toString();
        }
        return "payment fail";
    }
    
}
